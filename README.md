I. Initialize


	01. Install System package
	--------------------------------------------------------------------------
	~$ sudo apt-get update
	~$ sudo apt-get upgrade
	~$ sudo apt-get install vim ssh
	~$ sudo apt-get install git git-core
	~$ sudo apt-get install nginx openssl
	~$ sudo apt-get install postgresql
	~$ sudo apt-get install python-pip python-virtualenv
	~$ sudo apt-get install memcached libmemcached-dev
	~$ sudo apt-get build-dep python-psycopg2
	~$ sudo apt-get install virtualenvwrapper
	/* ~$ pip install virtualenvwrapper */ // commented.. since it gets error


	02. Generate ssh key and add it to bitbucket
	--------------------------------------------------------------------------
	~$ ssh-keygen
	~$ cat ~/.ssh/id_rsa.pub


	03. Configure git
	--------------------------------------------------------------------------
	~$ git config --global user.name "Adi Li"
	~$ git config --global user.email "lct009@ie.cuhk.edu.hk"
	~$ git config --global color.ui always
	~$ git config --global alias.co checkout
	~$ git config --global alias.st status


	04. Append the following line to ~/.bashrc
	--------------------------------------------------------------------------
	+++++++++++++++++++++++++++++++++++++++++++++++++++
	|                                                 |
	|export WORKON_HOME=${HOME}/.virtualenv           |
	|# source "/usr/local/bin/virtualenvwrapper.sh"   |
        |virtualenvwrapper_initialize                     |
	|                                                 |
	+++++++++++++++++++++++++++++++++++++++++++++++++++


	05. Make a directory for ${WORKON_HOME}
	--------------------------------------------------------------------------
	~$ mkdir -p ${WORKON_HOME}


	06. Execute ~/.bashrc once
	--------------------------------------------------------------------------
	~$ ~/.bashrc


	07. Start virtualenv via virtualwrapper
	--------------------------------------------------------------------------
	~$ mkvirtualenv csci4140


	08. Make a site directory (in this example is /home/adi/csci4140)
	--------------------------------------------------------------------------
	(csci4140)~$ mkdir /home/adi/csci4140 && cd /home/adi/csci4140
	(csci4140)csci4140$ mkdir django
	(csci4140)csci4140$ mkdir -p public/static
	(csci4140)csci4140$ mkdir -p public/media
	(csci4140)csci4140$ mkdir -p nginx/log/access/http
	(csci4140)csci4140$ mkdir -p nginx/log/access/https
	(csci4140)csci4140$ mkdir -p nginx/log/error/http
	(csci4140)csci4140$ mkdir -p nginx/log/error/https


	09. Git the django code to /home/adi/csci4140/django
	--------------------------------------------------------------------------
	(csci4140)csci4140$ cd django
	(csci4140)django$ git clone git@bitbucket.org:adi-li/csci-4140.git .


	10. Install required python packages
	--------------------------------------------------------------------------
	(csci4140)django$ pip install -r requirements.txt


	11. Modify csci4140/config_template.py to meets own use and rename to
	      csci4140/config.py


	12. Configure nginx


	13. Run server
	--------------------------------------------------------------------------
	(csci4140)django$ python manage.py run_gunicorn 127.0.0.1:8000 -w 4


II. Git commands


	Checkout to your own branch
	--------------------------------------------------------------------------
	(csci4140)django$ git co adi


	Check Git status
	--------------------------------------------------------------------------
	(csci4140)django$ git st


	Commit git code
	--------------------------------------------------------------------------
	(csci4140)django$ git add (file to be commited)
	(csci4140)django$ git commit -m '(comment)'


	Push to remote server
	--------------------------------------------------------------------------
	(csci4140)django$ git push origin adi


	Pull from remote server
	--------------------------------------------------------------------------
	(csci4140)django$ git pull


III. PostgreSQL commands


	Enter shell
	--------------------------------------------------------------------------
	(csci4140)django$ psql -U adi -h localhost csci4140
	OR
	(csci4140)django$ python manage.py dbshell


	Create User
	--------------------------------------------------------------------------
	csci4140=> CREATE USER csci4140 WITH ENCRYPTED PASSWORD 'NWo3fO*nk+dc';


	Alter User
	--------------------------------------------------------------------------
	csci4140=> ALTER USER csci4140 WITH ENCRYPTED PASSWORD 'password';


	Create Database
	--------------------------------------------------------------------------
	csci4140=> CREATE DATABASE csci4140 WITH OWNER = csci4140;



IV. Django Manage Commands


	Get all manage.py commands
	--------------------------------------------------------------------------
	(csci4140)django$ python manage.py help


	Sync database
	--------------------------------------------------------------------------
	(csci4140)django$ python manage.py syncdb


	Migrate change of database
	--------------------------------------------------------------------------
	(csci4140)django$ python manage.py migrate


	Create a super user
	--------------------------------------------------------------------------
	(csci4140)django$ python manage.py createsuperuser


	Run database shell
	--------------------------------------------------------------------------
	(csci4140)django$ python manage.py dbshell


	Run python shell
	--------------------------------------------------------------------------
	(csci4140)django$ python manage.py shell


	Start a new app 
	--------------------------------------------------------------------------
	(csci4140)django$ python manage.py startapp app_name
	(csci4140)django$ mv app_name apps/


	Collect static files for production serve
	--------------------------------------------------------------------------
	(csci4140)django$ python manage.py collectstatic --noinput


	Run DEV server
	--------------------------------------------------------------------------
	(csci4140)django$ python manage.py runserver 0.0.0.0:8000


	Run as gunicorn
	--------------------------------------------------------------------------
	(csci4140)django$ python manage.py run_gunicorn 127.0.0.0:8000
