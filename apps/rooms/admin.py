from django.contrib import admin

from utils.admin import IdHashAdmin

from rooms.models import GameRoom

class GameRoomAdmin(IdHashAdmin):
    list_display = ('id_hash', '__unicode__', 'expires', 'is_public', 'avaliable_golds', 'current_gold_picker',)


admin.site.register(GameRoom, GameRoomAdmin)
