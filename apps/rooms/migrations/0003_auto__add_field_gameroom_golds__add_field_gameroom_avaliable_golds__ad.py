# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'GameRoom.golds'
        db.add_column(u'rooms_gameroom', 'golds',
                      self.gf('json_field.fields.JSONField')(default='[]', null=True, blank=True),
                      keep_default=False)

        # Adding field 'GameRoom.avaliable_golds'
        db.add_column(u'rooms_gameroom', 'avaliable_golds',
                      self.gf('json_field.fields.JSONField')(default='[]', null=True, blank=True),
                      keep_default=False)

        # Adding field 'GameRoom.current_gold_picker'
        db.add_column(u'rooms_gameroom', 'current_gold_picker',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['saboteur.Player'], null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'GameRoom.golds'
        db.delete_column(u'rooms_gameroom', 'golds')

        # Deleting field 'GameRoom.avaliable_golds'
        db.delete_column(u'rooms_gameroom', 'avaliable_golds')

        # Deleting field 'GameRoom.current_gold_picker'
        db.delete_column(u'rooms_gameroom', 'current_gold_picker_id')


    models = {
        u'rooms.gameroom': {
            'Meta': {'ordering': "['-created']", 'object_name': 'GameRoom'},
            'avaliable_golds': ('json_field.fields.JSONField', [], {'default': "'[]'", 'null': 'True', 'blank': 'True'}),
            'closed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'current_gold_picker': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['saboteur.Player']", 'null': 'True', 'blank': 'True'}),
            'expires': ('django.db.models.fields.DateTimeField', [], {}),
            'golds': ('json_field.fields.JSONField', [], {'default': "'[]'", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_hash': ('django.db.models.fields.CharField', [], {'max_length': '50', 'unique': 'True', 'null': 'True', 'db_index': 'True'}),
            'is_public': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'saboteur.character': {
            'Meta': {'ordering': "['id']", 'object_name': 'Character'},
            'character_type': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'saboteur.player': {
            'Meta': {'ordering': "['-created']", 'object_name': 'Player'},
            'affected_cards': ('json_field.fields.JSONField', [], {'default': "'[]'", 'null': 'True', 'blank': 'True'}),
            'card_drawed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'cards': ('json_field.fields.JSONField', [], {'default': "'[]'", 'null': 'True', 'blank': 'True'}),
            'character': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['saboteur.Character']", 'null': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'gold_drawed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_hash': ('django.db.models.fields.CharField', [], {'max_length': '50', 'unique': 'True', 'null': 'True', 'db_index': 'True'}),
            'name': ('django.db.models.fields.SlugField', [], {'max_length': '10'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'room': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['rooms.GameRoom']", 'null': 'True'}),
            'score': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'session': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['rooms']