import random
import datetime

from django.db import models
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.utils.http import urlquote
from django.utils.timezone import now
from django.utils.translation import ugettext as _

from utils.models import IdHashModel
from json_field import JSONField

from saboteur.models import Game, Character, Card, Player


class PublicGameRoomManager(models.Manager):
    def __init__(self, *args, **kwargs):
        super(PublicGameRoomManager, self).__init__(*args, **kwargs)
        self._now = now()  # now timestamp cache

    def get_query_set(self):
        return super(PublicGameRoomManager, self).get_query_set().filter(
            is_public=True, expires__gt=self._now, closed=False)

    def get_all_avaliable(self):
        qs = self.annotate(num_players=models.Count('player'))
        qs = qs.annotate(num_games=models.Count('game'))
        qs = qs.filter(num_games=0)
        qs = qs.filter(num_players__lt=GameRoom.MAX_GAME_ROOM_MEMBER)
        return qs


class GameRoom(IdHashModel):
    MIN_GAME_ROOM_MEMBER = 3
    MAX_GAME_ROOM_MEMBER = 6
    MAX_GAME_NUMBER = 3
    INIT_TIME_TO_EXPIRE = 1  # hours

    QRCODE_BASE_URL = "http://chart.apis.google.com/chart?" \
                      "cht=qr&chs=300x300&chld=H|0&chl="

    expires = models.DateTimeField()
    is_public = models.BooleanField(default=False)
    closed = models.BooleanField(default=False)

    golds = JSONField(default="[]", null=True, blank=True)  # array of golds
    avaliable_golds = JSONField(default="[]", null=True, blank=True)  # array of avaliable golds for user to choose
    current_gold_picker = models.ForeignKey(Player, null=True, blank=True)


    objects = models.Manager()
    public_objects = PublicGameRoomManager()

    @property
    def started(self):
        return bool(self.game_set.count())

    @property
    def can_start(self):
        return bool(self.player_set.count() >= GameRoom.MIN_GAME_ROOM_MEMBER) and not self.closed

    @property
    def can_auto_start(self):
        return self.can_start and not self.started and self.is_full

    @property
    def is_expired(self):
        return bool(self.expires < now())

    @property
    def is_full(self):
        return bool(self.player_set.count() >= GameRoom.MAX_GAME_ROOM_MEMBER)

    @property
    def last_updated(self):
        return int(self.updated.strftime('%s'))

    def __unicode__(self):
        return unicode(self.id).zfill(5)

    def get_absolute_url(self):
        return reverse('game-room', args=(self.id_hash,))

    def current_game(self):
        return self.game_set.order_by('-created').latest()

    def get_qrcode(self, request, use_chrome=False):
        url = urlquote(request.build_absolute_uri(self.get_absolute_url()))
        if use_chrome:
            url = 'googlechrome' + url[4:]
        return GameRoom.QRCODE_BASE_URL + url

    def can_check_in(self, request, set_messages=True):
        ok_to_enter = True
        if self.closed or self.is_expired:
            messages.error(request, _('The room is closed'))
            return False

        if self.started:
            messages.error(request, _('The room is started.'))
            ok_to_enter = False

        if self.is_full:
            messages.error(request, _('The room is already full.'))
            ok_to_enter = False

        return ok_to_enter

    def get_players_dict(self):
        return map(lambda i: i.to_dict(),
                   self.player_set.order_by('position').all())

    def start_game(self):
        if not self.can_start:
            return False

        num_players = self.player_set.count()
        characters = Character.objects.random(num_players)
        num_dummy = Card.objects.get_num_dummy(num_players)
        game = Game.objects.create_game(self)

        if len(self.golds) <= 0:
            self.golds = [100] * 15
            self.golds += [200] * 10
            self.golds += [300] * 5
            random.shuffle(self.golds)

        for player in self.player_set.all():
            player.cards = game.get_cards(num_dummy)
            player.character = characters.pop()
            player.affected_cards = []
            player.gold_drawed = False
            player.card_drawed = False
            player.save()

        self.expires += datetime.timedelta(hours=1)
        self.save()
        return True

    def end_game(self):
        if not self.started:
            return False

    def handle_bad_guy_win(self):
        self.player_set.filter(
            character__character_type=Character.BAD_GUY
        ).update(score=models.F('score')+300)
        for i in xrange(0, self.player_set.filter(
                character__character_type=Character.BAD_GUY).count()):
            if 300 in self.golds:
                self.golds.remove(300)
                continue
            if 200 in self.golds and 100 in self.golds:
                self.golds.remove(200)
                self.golds.remove(100)
                continue
            if 100 in self.golds:
                self.golds.remove(100)
                self.golds.remove(100)
                self.golds.remove(100)
        self.save()

    def get_golds(self):
        self.avaliable_golds = self.golds[:6]
        self.golds = self.golds[6:]
