from django.conf.urls import patterns, include, url


urlpatterns = patterns(
    'rooms.views',
    url(r'^add$', 'add', name='room-add'),
    url(r'^in/(?P<room_id>\w+)$', 'room', name='game-room'),
)
