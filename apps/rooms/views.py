from django.http import HttpResponse
from django.contrib import messages
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext
from django.views.decorators.http import require_POST
from django.utils import simplejson as json
from django.utils.timezone import now
from django.utils.translation import ugettext as _
from datetime import timedelta

from rooms.models import GameRoom
from saboteur.auth import save_player, remove_player
from saboteur.models import Player, Card, GameMap, Character


@require_POST
def add(request):
    if request.is_mobile:
        return redirect('home')

    is_public = bool(request.POST.get('public', False))
    room = GameRoom.objects.create(
        expires=(now() + timedelta(days=GameRoom.INIT_TIME_TO_EXPIRE)),
        is_public=is_public
    )

    return redirect('game-room', room_id=room.id_hash)


def get_room_context_dict(request, room):
    cards = json.dumps(Card.objects.get_cards_dict())
    characters = json.dumps(Character.objects.get_characters_dict())
    maps = json.dumps(GameMap().to_dict())
    return {
        'room': room, 'cards': cards,
        'qrcode': room.get_qrcode(request),
        'qrcode_google': room.get_qrcode(request, True),
        'maps': maps, 'characters': characters,
    }


def room(request, room_id):
    """
    mobile => join room
    desktop => watch match
    """
    room = get_object_or_404(GameRoom, id_hash=room_id)

    template = 'games/desktop.html'

    if request.is_mobile:
        if request.method == 'POST' or request.player is not None:
            return mobile_enter_room_action(request, room)
        else:
            if not room.can_check_in(request):
                return redirect('home')
            template = 'games/mobile_before_enter.html'

    return render_to_response(
        template, get_room_context_dict(request, room),
        RequestContext(request))


def mobile_enter_room_action(request, room):
    player = request.player
    if player is None:  # session has no player
        if room.can_check_in(request):
            username = request.POST.get('username', None)
            if not username:
                messages.error(request, _('Please enter your name'))
                return redirect('game-room', room_id=room.id_hash)
            import re
            if re.match('^[-\w]{3,10}$', username) is None:
                messages.error(request, _('Invalid username'))
                return redirect('game-room', room_id=room.id_hash)
            request.session['default_name'] = username
            if Player.objects.filter(name=username, room=room).exists():
                messages.error(request, _('{username} has been used').format(
                    username=username))
                return redirect('game-room', room_id=room.id_hash)
            player = Player.objects.create_player(request, room, username)
        else:
            return redirect('home')
    elif player.room.id != room.id:  # player does not belong to this room
        return redirect('game-room', room_id=player.room.id_hash)

    if room.is_expired:
        remove_player(request, room)
        return redirect('home')

    save_player(request, player)

    if room.can_auto_start:
        room.start_game()

    room.save()

    context = get_room_context_dict(request, room)
    context['player'] = request.player
    context['room_url'] = request.build_absolute_uri(room.get_absolute_url())

    return render_to_response(
        'games/mobile.html', context,
        RequestContext(request))
