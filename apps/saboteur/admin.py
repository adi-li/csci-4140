from django.contrib import admin

from utils.admin import BaseAdmin, IdHashAdmin

from saboteur.models import Card, Character, Player, Game


class CardAdmin(BaseAdmin):
    list_display = ('id', 'card_type', 'name', 'attributes',)
    list_per_page = 100


class CharacterAdmin(BaseAdmin):
    list_display = ('id', 'character_type', 'name',)
    list_per_page = 20


class PlayerAdmin(BaseAdmin):
    list_display = ('name', 'room', 'position', 'cards', 'affected_cards', 'character', 'score',)


class GameAdmin(BaseAdmin):
    list_display = ('id', 'room', 'current_turn', 'gold_position', 'game_map',
        'is_game_end', 'is_good_guy_win',)


admin.site.register(Card, CardAdmin)
admin.site.register(Character, CharacterAdmin)
admin.site.register(Player, PlayerAdmin)
admin.site.register(Game, GameAdmin)
