from django.utils.timezone import now
from django.utils.translation import ugettext as _

from saboteur.models import Game, Player, Card, Character, GameMap
from saboteur.auth import remove_player
from rooms.models import GameRoom

from datetime import timedelta
from time import sleep


def waiting(request, result, room):
    client_last_update = int(request.POST.get('last_updated', 0))

    result['success'] = True
    temp_result = {}

    expires = now() + timedelta(seconds=30)

    while now() < expires:
        result['last_updated'] = room.last_updated

        if room.last_updated == client_last_update:
            sleep(1)
            room = GameRoom.objects.get(id=room.id)
        else:
            temp_result['room'] = {
                'started': room.started,
                'can_start': room.can_start,
                'num_games': room.game_set.count()
            }
            temp_result['players'] = room.get_players_dict()
            break

    if result['last_updated'] != client_last_update:
        result.update(temp_result)

    return result


def waiting_next(request, result, room):
    client_last_update = int(request.POST.get('last_updated', 0))
    client_num_games = int(request.POST.get('num_games', 0))

    result['success'] = True
    result['close_room'] = room.closed

    expires = now() + timedelta(seconds=30)

    while now() < expires:
        result['num_games'] = room.game_set.count()
        result['close_room'] = room.closed
        result['avaliable_golds'] = room.avaliable_golds
        result['last_updated'] = room.last_updated
        if room.current_gold_picker:
            result['picker'] = room.current_gold_picker.position
        else:
            result['picker'] = None

        if result['last_updated'] == client_last_update and \
                result['num_games'] == client_num_games:
            sleep(1)
            room = GameRoom.objects.get(id=room.id)
        else:
            break

    return result


def exit_room(request, result, room):
    result['success'] = True
    if request.player is not None and \
            request.player.room.id_hash == room.id_hash:
        remove_player(request, room)
        if room.started:
            room.closed = True
        room.save()

    return result


def start_game(request, result, room):
    if request.player.position == 1:
        result['success'] = room.start_game()
    return result


def refresh(request, result, room):
    last_updated = int(request.POST.get('last_updated', 0))
    expires = now() + timedelta(seconds=30)
    result['success'] = True
    temp_result = {}
    while now() < expires:
        game = room.current_game()

        result['last_updated'] = game.last_updated()

        if last_updated == result['last_updated']:
            sleep(1)
            room = GameRoom.objects.get(id=room.id)
            if request.is_mobile and request.player is not None and \
                    request.player.room_id == room.id:
                request.player = Player.objects.get(id=request.player.id)
        else:
            temp_result['num_games'] = room.game_set.count()
            temp_result['num_rounds'] = game.current_turn
            temp_result['cards_deck_left'] = len(game.cards_deck)
            temp_result['is_game_end'] = game.is_game_end
            temp_result['is_good_guy_win'] = game.is_good_guy_win
            temp_result['map'] = game.game_map
            temp_result['players'] = room.get_players_dict()
            if request.is_mobile and request.player is not None and \
                    request.player.room_id == room.id:
                temp_result['myself'] = request.player.to_dict(True)
            break

    if last_updated != result['last_updated']:
        result.update(temp_result)
    return result


def play_card(request, result, room):
    card = int(request.POST.get('card_id', 0))
    card = request.player.card_in_hand(card)
    x = int(request.POST.get('x', -1))
    y = int(request.POST.get('y', -1))
    player_position = int(request.POST.get('player_position', -1))
    is_reversed = bool(int(request.POST.get('reversed', 0)))

    game = room.current_game()
    if not game.is_his_turn(request.player.position):
        return result

    if game.is_game_end:
        result['message'] = _('Game is ended')
        return result

    if card is not None:
        maps = GameMap(game.game_map)
        if card.card_type == Card.ROAD_TYPE:
            if len(request.player.affected_cards) > 0:
                result['message'] = _('You are being blocked')
                return result
            if x < 0 or y < 0 or x >= GameMap.COL or y >= GameMap.ROW:
                return result
            if x == 0 and y == 2:
                result['message'] = _('This is starting point')
                return result
            if x == 8 and y in [0, 2, 4]:
                result['message'] = _('This is destination')
                return result
            if not maps.is_buildable(x, y, card.id, is_reversed):
                result['message'] = _('It is not buildable')
            else:
                game.game_map[x][y]['card'] = card.id
                game.game_map[x][y]['reversed'] = is_reversed
                result['success'] = True

        elif card.card_type == Card.UNBLOCK_TOOL_TYPE:
            if player_position <= room.player_set.count():
                if player_position == request.player.position:
                    target = request.player
                else:
                    target = room.player_set.get(position=player_position)
                affected_cards = Card.objects.filter(id__in=target.affected_cards)
                for affected_card in affected_cards:
                    temp_attributes = {
                        'Type1': card.attributes['Type1'],
                        'Type2': card.attributes['Type2'],
                        'Type3': card.attributes['Type3'],
                    }
                    if affected_card.attributes['Type1'] and temp_attributes['Type1']:
                        temp_attributes['Type1'] = False
                        affected_card.attributes['Type1'] = False
                    if affected_card.attributes['Type2'] and temp_attributes['Type2']:
                        temp_attributes['Type2'] = False
                        affected_card.attributes['Type2'] = False
                    if affected_card.attributes['Type3'] and temp_attributes['Type3']:
                        temp_attributes['Type3'] = False
                        affected_card.attributes['Type3'] = False

                    if all(val is False for val in affected_card.attributes.itervalues()):
                        target.affected_cards.remove(affected_card.id)
                        target.save()
                        card.attributes = temp_attributes

                    if all(val is False for val in card.attributes.itervalues()):
                        break
                result['success'] = True

        elif card.card_type == Card.BLOCK_TOOL_TYPE:
            if player_position != request.player.position and \
                    player_position <= room.player_set.count():
                if player_position == request.player.position:
                    target = request.player
                else:
                    target = room.player_set.get(position=player_position)
                target.affected_cards.append(card.id)
                target.save()
                result['success'] = True

        elif card.card_type == Card.READ_MAP_TOOL_TYPE:
            if x != (GameMap.COL - 1) or y not in [Game.GOLD_POSTITION_TOP,
                                                   Game.GOLD_POSTITION_MIDDLE,
                                                   Game.GOLD_POSTITION_BOTTOM]:
                return result
            result['message'] = _('There is no gold')
            if y == game.gold_position:
                result['message'] = _('There is a GOLD!!!!')
            result['success'] = True

        elif card.card_type == Card.BREAK_ROAD_TOOL_TYPE:
            if x < 0 or y < 0 or x >= GameMap.COL or y >= GameMap.ROW:
                return result
            if x == 0 and y == 2:
                result['message'] = _('Starting point cannot be borken')
                return result
            if x == (GameMap.COL-1) and (y in [
                    Game.GOLD_POSTITION_TOP, Game.GOLD_POSTITION_MIDDLE, Game.GOLD_POSTITION_BOTTOM]):
                result['message'] = _('Destinations cannot be borken')
                return result
            if game.game_map[x][y]['card'] is None:
                result['message'] = _('There is no road can be borken')
                return result
            game.game_map[x][y]['card'] = None
            result['success'] = True

    if result['success']:
        request.player.cards.remove(card.id)
        request.player.save()
        game.is_game_end, game.is_good_guy_win = game.check_game_end()
        if game.is_game_end:
            if game.is_good_guy_win and \
                    request.player.character.character_type == Character.GOOD_GUY:
                room.current_gold_picker = request.player
                room.get_golds()
            else:
                room.handle_bad_guy_win()
        else:
            game.current_turn += 1
        game.save()
        room.save()
        room.player_set.update(card_drawed=False)

    return result


def discard_card(request, result, room):
    game = room.current_game()
    if not game.is_his_turn(request.player.position):
        return result

    if game.is_game_end:
        result['message'] = _('Game is ended')
        return result

    card = int(request.POST.get('card_id', 0))
    card = request.player.card_in_hand(card)
    if card is not None:
        game = room.current_game()
        request.player.cards.remove(card.id)
        request.player.save()
        game.is_game_end, game.is_good_guy_win = game.check_game_end()
        if game.is_game_end:
            if not game.is_good_guy_win:
                room.handle_bad_guy_win()
        else:
            game.current_turn += 1
        game.save()
        room.player_set.update(card_drawed=False)
    result['success'] = True
    return result


def draw_card(request, result, room):
    game = room.current_game()
    if game.current_turn <= 1 or request.player.card_drawed or \
            len(game.cards_deck) <= 0:
        return result
    num_players = room.player_set.count()
    draw_position = (game.current_turn-1) % num_players
    if draw_position == 0:
        draw_position = num_players
    if draw_position == request.player.position and \
            Card.objects.get_num_dummy(num_players) > len(request.player.cards):
        cards = game.get_cards()
        if cards:
            request.player.cards += cards
            result['card_id'] = cards
            result['success'] = True
        request.player.card_drawed = True
        request.player.save()
    return result


def draw_gold(request, result, room):
    gold_num = int(request.POST.get('gold_num', 0))
    game = room.current_game()
    if room.current_gold_picker.id != request.player.id:
        return result
    if gold_num not in room.avaliable_golds:
        return result
    request.player.score += gold_num
    room.avaliable_golds.remove(gold_num)
    if len(room.avaliable_golds) <= 0:
        room.current_gold_picker = None
    else:
        players = list(room.player_set.order_by('position'))
        current_pos = (room.current_gold_picker.position) % room.player_set.count()
        players = players[current_pos:] + players[:current_pos]
        for p in players:
            if p.character.character_type == Character.GOOD_GUY and not p.affected_cards:
                room.current_gold_picker = p
                break
    request.player.save()
    game.save()
    room.save()
    result['success'] = True
    return result


def pass_game(request, result, room):
    game = room.current_game()
    if not game.is_his_turn(request.player.position):
        return result

    if game.is_game_end:
        result['message'] = _('Game is ended')
        return result

    if len(request.player.cards) > 0:
        return result

    result['success'] = True
    game.current_turn += 1
    game.save()
    return result
