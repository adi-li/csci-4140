from saboteur.models import Player

PLAYER_SESSION_KEY = '__PlayerSessionKey__'


def save_player(request, player):
    request.session[PLAYER_SESSION_KEY] = player.id
    request.player = player
    return request


def remove_player(request, room):
    if PLAYER_SESSION_KEY in request.session:
        if not room.started:
            request.player.delete()
        request.player = None
        del request.session[PLAYER_SESSION_KEY]
    return request


def get_player(request):
    player_id = request.session.get(PLAYER_SESSION_KEY, None)
    if player_id is not None and Player.objects.filter(id=player_id).exists():
        return Player.objects.get(id=player_id)
