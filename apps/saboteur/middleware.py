from saboteur.auth import get_player


class SaboteurPlayerMiddleware(object):
    def process_request(self, request):
        request.player = get_player(request)
        return None
