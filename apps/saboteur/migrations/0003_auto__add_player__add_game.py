# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Player'
        db.create_table(u'saboteur_player', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('id_hash', self.gf('django.db.models.fields.CharField')(max_length=50, unique=True, null=True, db_index=True)),
            ('room', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['rooms.GameRoom'], null=True)),
            ('cards', self.gf('json_field.fields.JSONField')(default='[]')),
            ('affected_cards', self.gf('json_field.fields.JSONField')(default='[]')),
            ('character', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['saboteur.Character'])),
            ('score', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
        ))
        db.send_create_signal(u'saboteur', ['Player'])

        # Adding model 'Game'
        db.create_table(u'saboteur_game', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('room', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['rooms.GameRoom'])),
            ('current_turn', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('gold_position', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('game_map', self.gf('json_field.fields.JSONField')(default=u'null')),
        ))
        db.send_create_signal(u'saboteur', ['Game'])


    def backwards(self, orm):
        # Deleting model 'Player'
        db.delete_table(u'saboteur_player')

        # Deleting model 'Game'
        db.delete_table(u'saboteur_game')


    models = {
        u'rooms.gameroom': {
            'Meta': {'ordering': "['-created']", 'object_name': 'GameRoom'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'expires': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_hash': ('django.db.models.fields.CharField', [], {'max_length': '50', 'unique': 'True', 'null': 'True', 'db_index': 'True'}),
            'is_public': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'saboteur.card': {
            'Meta': {'ordering': "['id']", 'object_name': 'Card'},
            'attributes': ('json_field.fields.JSONField', [], {'default': "u'null'"}),
            'card_type': ('django.db.models.fields.CharField', [], {'max_length': '4'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'saboteur.character': {
            'Meta': {'ordering': "['id']", 'object_name': 'Character'},
            'character_type': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'saboteur.game': {
            'Meta': {'ordering': "['-created']", 'object_name': 'Game'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'current_turn': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'game_map': ('json_field.fields.JSONField', [], {'default': "u'null'"}),
            'gold_position': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'room': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['rooms.GameRoom']"}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'saboteur.player': {
            'Meta': {'ordering': "['-created']", 'object_name': 'Player'},
            'affected_cards': ('json_field.fields.JSONField', [], {'default': "'[]'"}),
            'cards': ('json_field.fields.JSONField', [], {'default': "'[]'"}),
            'character': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['saboteur.Character']"}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_hash': ('django.db.models.fields.CharField', [], {'max_length': '50', 'unique': 'True', 'null': 'True', 'db_index': 'True'}),
            'room': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['rooms.GameRoom']", 'null': 'True'}),
            'score': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['saboteur']