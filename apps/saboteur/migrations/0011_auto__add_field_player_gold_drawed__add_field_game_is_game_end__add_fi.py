# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Player.gold_drawed'
        db.add_column(u'saboteur_player', 'gold_drawed',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Game.is_game_end'
        db.add_column(u'saboteur_game', 'is_game_end',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Game.is_good_guy_win'
        db.add_column(u'saboteur_game', 'is_good_guy_win',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Game.golds'
        db.add_column(u'saboteur_game', 'golds',
                      self.gf('json_field.fields.JSONField')(default='[]', null=True, blank=True),
                      keep_default=False)

        # Adding field 'Game.avaliable_golds'
        db.add_column(u'saboteur_game', 'avaliable_golds',
                      self.gf('json_field.fields.JSONField')(default='[]', null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Player.gold_drawed'
        db.delete_column(u'saboteur_player', 'gold_drawed')

        # Deleting field 'Game.is_game_end'
        db.delete_column(u'saboteur_game', 'is_game_end')

        # Deleting field 'Game.is_good_guy_win'
        db.delete_column(u'saboteur_game', 'is_good_guy_win')

        # Deleting field 'Game.golds'
        db.delete_column(u'saboteur_game', 'golds')

        # Deleting field 'Game.avaliable_golds'
        db.delete_column(u'saboteur_game', 'avaliable_golds')


    models = {
        u'rooms.gameroom': {
            'Meta': {'ordering': "['-created']", 'object_name': 'GameRoom'},
            'closed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'expires': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_hash': ('django.db.models.fields.CharField', [], {'max_length': '50', 'unique': 'True', 'null': 'True', 'db_index': 'True'}),
            'is_public': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'saboteur.card': {
            'Meta': {'ordering': "['id']", 'object_name': 'Card'},
            'attributes': ('json_field.fields.JSONField', [], {'default': "u'null'"}),
            'card_type': ('django.db.models.fields.CharField', [], {'max_length': '4'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'saboteur.character': {
            'Meta': {'ordering': "['id']", 'object_name': 'Character'},
            'character_type': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'saboteur.game': {
            'Meta': {'ordering': "['-created']", 'object_name': 'Game'},
            'avaliable_golds': ('json_field.fields.JSONField', [], {'default': "'[]'", 'null': 'True', 'blank': 'True'}),
            'cards_deck': ('json_field.fields.JSONField', [], {'default': "'[]'", 'null': 'True', 'blank': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'current_turn': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'game_map': ('json_field.fields.JSONField', [], {'default': "u'null'"}),
            'gold_position': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'golds': ('json_field.fields.JSONField', [], {'default': "'[]'", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_game_end': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_good_guy_win': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'room': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['rooms.GameRoom']"}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'saboteur.player': {
            'Meta': {'ordering': "['-created']", 'object_name': 'Player'},
            'affected_cards': ('json_field.fields.JSONField', [], {'default': "'[]'", 'null': 'True', 'blank': 'True'}),
            'card_drawed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'cards': ('json_field.fields.JSONField', [], {'default': "'[]'", 'null': 'True', 'blank': 'True'}),
            'character': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['saboteur.Character']", 'null': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'gold_drawed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_hash': ('django.db.models.fields.CharField', [], {'max_length': '50', 'unique': 'True', 'null': 'True', 'db_index': 'True'}),
            'name': ('django.db.models.fields.SlugField', [], {'max_length': '10'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'room': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['rooms.GameRoom']", 'null': 'True'}),
            'score': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'session': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['saboteur']