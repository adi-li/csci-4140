import random

from django.core.cache import cache
from django.conf import settings
from django.db import models
from django.utils.translation import ugettext as _

from json_field import JSONField

from utils.models import BaseModel, IdHashModel


class CardManager(models.Manager):
    def get_cards_dict(self):
        cache_key = 'CardManager::get_cards_dict'
        version = 1
        cards = cache.get(cache_key, version=version)
        if cards is None:
            cards = map(lambda i: i.to_dict(), self.all())
            cache.set(cache_key, cards, 0, version=version)
        return cards

    def get_num_dummy(self, num_players):
        if num_players <= 5:
            return 6
        elif num_players <= 7:
            return 5
        else:
            return 4


class Card(BaseModel):
    """
    attributes field vary with the card type
    ==>> road type: {
    ==>>    North   : bool,
    ==>>    East    : bool,
    ==>>    South   : bool,
    ==>>    West    : bool,
    ==>>    Blocked : bool
    ==>> }
    ==>> ###############################
    ==>> block tool type: {
    ==>>    TYPE1: bool,
    ==>>    TYPE2: bool,
    ==>>    TYPE3: bool,
    ==>> }
    ==>> ###############################
    ==>> unblock tool type: {
    ==>>    TYPE1: bool,
    ==>>    TYPE2: bool,
    ==>>    TYPE3: bool,
    ==>> }
    ==>> ###############################
    ==>> readmap tool type
    ==>> ###############################
    ==>> break road tool type
    ==>> ###############################
    """
    INIT_CARD_ID = 1
    DEST_CARD_ID = 69
    GOLD_CARD_ID = 70
    STONE_CARD_ID = 71
    ROAD_TYPE = 'ROAD'
    BLOCK_TOOL_TYPE = 'BLCK'
    UNBLOCK_TOOL_TYPE = 'UBLK'
    READ_MAP_TOOL_TYPE = 'RDMP'
    BREAK_ROAD_TOOL_TYPE = 'BKRD'
    CARD_TYPE_CHOICES = (
        (ROAD_TYPE, 'Road'),
        (BLOCK_TOOL_TYPE, 'Block Tool'),
        (UNBLOCK_TOOL_TYPE, 'Unblock Tool'),
        (READ_MAP_TOOL_TYPE, 'Read Map Tool'),
        (BREAK_ROAD_TOOL_TYPE, 'Break Road Tool'),
    )

    card_type = models.CharField(max_length=4,
                                 choices=CARD_TYPE_CHOICES)
    name = models.CharField(max_length=50)
    attributes = JSONField()

    objects = CardManager()

    @property
    def image_path(self):
        return "{0}games/images/cards/{1}.png".format(settings.STATIC_URL, self.id)

    class Meta(BaseModel.Meta):
        ordering = ['id']

    def __unicode__(self):
        return self.name

    def to_dict(self):
        return {
            'id': self.id,
            'card_type': self.card_type,
            'attributes': self.attributes,
            'image_path': self.image_path
        }

    @classmethod
    def get_types(cls):
        return {
            'ROAD_TYPE': cls.ROAD_TYPE,
            'BLOCK_TOOL_TYPE': cls.BLOCK_TOOL_TYPE,
            'UNBLOCK_TOOL_TYPE': cls.UNBLOCK_TOOL_TYPE,
            'READ_MAP_TOOL_TYPE': cls.READ_MAP_TOOL_TYPE,
            'BREAK_ROAD_TOOL_TYPE': cls.BREAK_ROAD_TOOL_TYPE
        }


class CharacterManager(models.Manager):
    def get_characters_dict(self):
        cache_key = 'CharacterManager::get_characters_dict'
        version = 1
        characters = cache.get(cache_key, version=version)
        if characters is None:
            characters = map(lambda i: i.to_dict(), self.all())
            cache.set(cache_key, characters, 0, version=version)
        return characters

    def random(self, num_players):
        characters = list(self.all())
        good_guys = characters[:7]
        bad_guys = characters[7:]
        random.shuffle(good_guys)
        random.shuffle(bad_guys)
        if num_players == 3:
            result = good_guys[:3] + bad_guys[:1]
        elif num_players == 4:
            result = good_guys[:4] + bad_guys[:1]
        elif num_players == 5:
            result = good_guys[:4] + bad_guys[:2]
        elif num_players == 6:
            result = good_guys[:5] + bad_guys[:2]
        elif num_players == 7:
            result = good_guys[:5] + bad_guys[:3]
        elif num_players == 8:
            result = good_guys[:6] + bad_guys[:3]
        elif num_players == 9:
            result = good_guys[:7] + bad_guys[:3]
        elif num_players == 10:
            result = good_guys[:7] + bad_guys[:4]
        random.shuffle(result)
        return result


class Character(BaseModel):
    GOOD_GUY = 'GD'
    BAD_GUY = 'BD'
    TYPE_CHOICES = (
        (GOOD_GUY, 'Good Guy'),
        (BAD_GUY, 'Bad Guy'),
    )

    character_type = models.CharField(max_length=2,
                                      choices=TYPE_CHOICES)
    name = models.CharField(max_length=20)

    objects = CharacterManager()

    @property
    def image_path(self):
        return "{0}games/images/characters/{1}.jpg".format(settings.STATIC_URL, self.id)

    class Meta(BaseModel.Meta):
        ordering = ['id']

    def __unicode__(self):
        return self.name

    def to_dict(self):
        return {
            'name': self.name,
            'character_type': self.character_type,
            'image_path': self.image_path
        }


class GameMap(object):
    """
    (9 x 5) table
    [{coordinate:{x, y}, card, reversed},]
    """
    NORTH = 'North'
    SOUTH = 'South'
    EAST = 'East'
    WEST = 'West'
    COL = 9
    ROW = 5

    def __init__(self, map_dict=None):
        super(GameMap, self).__init__()
        if map_dict is None:
            self.resetMap()
        else:
            self._maps = map_dict
        self.cards = Card.objects.get_cards_dict()

    def resetMap(self):
        self._maps = []
        for x in xrange(0, GameMap.COL):
            column = []
            for y in xrange(0, GameMap.ROW):
                obj = {
                    'coordinate': {'x': x, 'y': y},
                    'card': None,
                    'reversed': False,
                }
                column.append(obj)
            self._maps.append(column)

        self._maps[0][2]['card'] = Card.INIT_CARD_ID
        # self._maps[GameMap.COL-1][0]['card'] = Card.DEST_CARD_ID
        # self._maps[GameMap.COL-1][2]['card'] = Card.DEST_CARD_ID
        # self._maps[GameMap.COL-1][4]['card'] = Card.DEST_CARD_ID

    def to_dict(self):
        return self._maps

    def is_reachable(self, x, y):           # detect W, N, S, E
        def _check_linkable(map_cell, direction):
            if map_cell is None or map_cell['card'] is None:
                return False

            card = self.get_final_direction(map_cell['card'], map_cell['reversed'])
            if card[direction] and not card['Blocked']:
                return _reachable(map_cell['coordinate']['x'], map_cell['coordinate']['y'], skip=direction)

        def _reachable(x, y, skip=None):
            if self._maps[x][y]['card'] == Card.INIT_CARD_ID:
                return True

            left, right, up, down = self.find_neighbours(x, y)

            reached = False
            if not reached and skip != GameMap.WEST:
                reached = _check_linkable(left, GameMap.EAST)
            if not reached and skip != GameMap.NORTH:
                reached = _check_linkable(up, GameMap.SOUTH)
            if not reached and skip != GameMap.SOUTH:
                reached = _check_linkable(down, GameMap.NORTH)
            if not reached and skip != GameMap.NORTH:
                reached = _check_linkable(right, GameMap.WEST)
            return reached

        return _reachable(x, y)

    def find_neighbours(self, x, y):
        left = x - 1 if x > 0 else False
        right = x + 1 if x < GameMap.COL-1 else False
        up = y - 1 if y > 0 else False
        down = y + 1 if y < GameMap.ROW-1 else False

        left = None if left is False else self._maps[left][y]
        right = None if right is False else self._maps[right][y]
        up = None if up is False else self._maps[x][up]
        down = None if down is False else self._maps[x][down]

        return (left, right, up, down)

    def is_buildable(self, x, y, card_id, is_reversed):
        def _check_linkable(map_cell, direction):
            card = self.get_final_direction(map_cell['card'], map_cell['reversed'])
            return card[direction]

        if self._maps[x][y]['card'] is not None or not self.is_reachable(x, y):
            return False
        card = self.get_final_direction(card_id, is_reversed)

        left, right, up, down = self.find_neighbours(x, y)

        buildable = True
        if buildable and left is not None and left['card'] is not None:
            buildable = _check_linkable(left, GameMap.EAST) is card[GameMap.WEST]
        if buildable and up is not None and up['card'] is not None:
            buildable = _check_linkable(up, GameMap.SOUTH) is card[GameMap.NORTH]
        if buildable and down is not None and down['card'] is not None:
            buildable = _check_linkable(down, GameMap.NORTH) is card[GameMap.SOUTH]
        if buildable and right is not None and right['card'] is not None:
            buildable = _check_linkable(right, GameMap.WEST) is card[GameMap.EAST]
        return buildable

    def get_final_direction(self, card_id, is_reversed):
        card = self.cards[card_id-1]
        return {
            GameMap.NORTH: card['attributes'][GameMap.NORTH] if not is_reversed else card['attributes'][GameMap.SOUTH],
            GameMap.EAST: card['attributes'][GameMap.EAST] if not is_reversed else card['attributes'][GameMap.WEST],
            GameMap.SOUTH: card['attributes'][GameMap.SOUTH] if not is_reversed else card['attributes'][GameMap.NORTH],
            GameMap.WEST: card['attributes'][GameMap.WEST] if not is_reversed else card['attributes'][GameMap.EAST],
            'Blocked': card['attributes']['Blocked']
        }


class PlayerManager(models.Manager):
    def create_player(self, request, room, default_name):
        if not request.session.exists(request.session.session_key):
            request.session.create()
        return Player.objects.create(
            room=room,
            name=default_name,
            session=request.session.session_key,
            position=(room.player_set.count()+1),
        )


class Player(IdHashModel):
    room = models.ForeignKey('rooms.GameRoom', null=True)
    session = models.CharField(max_length=50)  # maybe change into link with user in future
    name = models.SlugField(max_length=10)
    position = models.PositiveSmallIntegerField()
    cards = JSONField(default="[]", null=True, blank=True)  # array of card ids
    affected_cards = JSONField(default="[]", null=True, blank=True)  # array of affected cards such as block tool
    card_drawed = models.BooleanField(default=False)
    gold_drawed = models.BooleanField(default=False)
    character = models.ForeignKey(Character, null=True)
    score = models.PositiveIntegerField(default=0)

    objects = PlayerManager()

    def __unicode__(self):
        return _('{name} - {room}').format(name=self.name, room=self.room.id)

    @property
    def image_path(self):
        return "{0}games/images/players/{1}.jpg".format(settings.STATIC_URL, self.position)

    def to_dict(self, get_detail=False):
        _dict = {
            'position': self.position,
            'affected_cards': self.affected_cards,
            'score': self.score,
            'name': self.name,
            'image_path': self.image_path,
            'cards_left': len(self.cards),
            'updated': int(self.updated.strftime('%s')),
        }

        if get_detail:
            _dict['character'] = self.character_id
            _dict['cards'] = self.cards
            _dict['is_my_turn'] = self.room.current_game().is_his_turn(self.position)

        return _dict

    def reset(self):
        self.cards = []
        self.affected_cards = []
        self.character = None
        self.gold_drawed = False
        self.card_drawed = False
        self.save()

    def card_in_hand(self, card_id):
        if card_id not in self.cards:
            return None
        card = Card.objects.get(id=card_id)
        return card


class GameManager(models.Manager):
    def create_game(self, room):
        cards = Card.objects.get_cards_dict()[Card.INIT_CARD_ID:(Card.DEST_CARD_ID-1)]  # shift out the first card
        random.shuffle(cards)
        gold_positions = [Game.GOLD_POSTITION_TOP, Game.GOLD_POSTITION_MIDDLE, Game.GOLD_POSTITION_BOTTOM]
        return self.create(
            room=room,
            gold_position=random.choice(gold_positions),
            cards_deck=cards,
            game_map=GameMap().to_dict()
        )


class Game(BaseModel):
    GOLD_POSTITION_TOP = 0
    GOLD_POSTITION_MIDDLE = 2
    GOLD_POSTITION_BOTTOM = 4
    GOLD_POSTITION_CHOICES = (
        (GOLD_POSTITION_TOP, 'Top'),
        (GOLD_POSTITION_MIDDLE, 'Middle'),
        (GOLD_POSTITION_BOTTOM, 'Bottom'),
    )

    INIT_NUMBER_CARDS = (
        (5, 6),  # max number of player, cards in hand
        (7, 5),
        (10, 4),
    )

    room = models.ForeignKey('rooms.GameRoom')
    is_game_end = models.BooleanField(default=False)
    is_good_guy_win = models.BooleanField(default=False)
    current_turn = models.PositiveSmallIntegerField(default=1)
    gold_position = models.PositiveSmallIntegerField(
        choices=GOLD_POSTITION_CHOICES)
    game_map = JSONField()  # GameMap object
    cards_deck = JSONField(default="[]", null=True, blank=True)  # array of cards_deck

    objects = GameManager()

    def last_updated(self):
        return int(self.updated.strftime('%s'))

    def get_cards(self, num=1):
        cards = self.cards_deck[:num]
        self.cards_deck = self.cards_deck[num:]
        self.save()
        cards = [i['id'] for i in cards]
        return cards

    def is_his_turn(self, position):
        player_count = self.room.player_set.count()
        if player_count <= 0:
            return False
        modulus = self.current_turn % player_count
        if modulus == 0:
            modulus = player_count
        return bool(modulus == position)

    def check_game_end(self):
        game_map = GameMap(self.game_map)
        x = GameMap.COL - 1
        if self.game_map[x][0]['card'] is None and game_map.is_reachable(x, 0):
            self.game_map[x][0]['card'] = Card.STONE_CARD_ID
        if self.game_map[x][2]['card'] is None and game_map.is_reachable(x, 2):
            self.game_map[x][2]['card'] = Card.STONE_CARD_ID
        if self.game_map[x][4]['card'] is None and game_map.is_reachable(x, 4):
            self.game_map[x][4]['card'] = Card.STONE_CARD_ID

        if self.game_map[x][self.gold_position]['card'] == Card.STONE_CARD_ID:
            self.game_map[x][self.gold_position]['card'] = Card.GOLD_CARD_ID
            return True, True
        if len(self.cards_deck) <= 0:
            for player in self.room.player_set.all():
                if len(player.cards) > 0:
                    return False, False
            return True, False
        return False, False


from django.db.models.signals import post_delete


def player_post_delete(sender, instance, using, **kwargs):
    room_id = instance.room_id
    position = instance.position
    exist_players = sender.objects.filter(room_id=room_id)
    if len(exist_players) > 0:
        if not exist_players[0].room.end_game():
            for player in exist_players:
                if player.position > position:
                    player.position -= 1
                    player.save()
        exist_players[0].room.save()

post_delete.connect(player_post_delete, sender=Player)
