var render_engine = {
	player: {
		template: _.template($('#playerDivTemplate').html()),
		draw: function(players) {
			$('.gameRoomMemberNumber').text(players.length);
			$('.player-div').each(function(){
				var myself = $(this);
				var position = myself.attr('data-position');
				var updated = myself.attr('data-updated');
				var i = position -1;
				if (players[i]) {
					if (updated != players[i].updated) {
						myself.html(render_engine.player.template(players[i]));
						myself.attr('data-updated', players[i].updated);
					}
				} else {
					myself.empty().attr('data-updated', null);
				}
			});
		}
	},

	map: {
		get_image: function (id, reversed) {
			if (!id) {
				return '';
			}
			var img = $('<img>');
			if (id > 0 && id < 68) {
				img.attr('src', CARDS[(id-1)].image_path);
			} else {
				img.attr('src', '/static/games/images/cards/'+id+'.png')
			}
			if (reversed) {
				img.addClass('img-flipped');
			}
			return img;
		},

		draw: function(map) {
			$('#gameMap .game-map-road-cell').each(function(){
				var myself = $(this);
				var x = myself.attr('data-x');
				var y = myself.attr('data-y');
				var card = myself.attr('data-card');
				do_update = true;
				if (map[x][y].card == card) {
					do_update = false;
				}
				if (do_update) {
					myself.empty().html(render_engine.map.get_image(map[x][y].card, map[x][y].reversed));
					myself.attr('data-card', map[x][y].card);
				}
			});
		}
	},

	myself: {
		draw: function(myself) {
			render_engine.myself.add_cards(myself.cards);
			if ($('#myRole img').length <= 0) {
				var img = $('<img>').attr('src', CHARS[myself.character-1].image_path);
				img.appendTo('#myRole');
			}
		},

		add_cards: function(cards) {
			if (cards.length == 0) {
				$('#myCards').hide();
				$('.passBtn').parents('.ui-btn').show();
				return;
			} else {
				$('.passBtn').parents('.ui-btn').hide();
				$('#myCards').show();
			}
			for (var i = 0; i < cards.length; i++) {
				if ($('#myCards li[data-id="'+cards[i]+'"]').length <= 0) {
					var li = $('<li>').addClass('cards').attr('data-id', cards[i]);
					$('<img>').attr('src', CARDS[cards[i]-1].image_path).appendTo(li);
					li.appendTo('#myCards');
					li.draggable({	// tap drag card
						revert: 'invalid',
						containment: "body",
						scroll: false,
						helper: 'clone',
						appendTo: 'body',
						start: function (e, ui) {
							$(e.target).css('visibility', 'hidden');
						}, stop: function (e, ui) {
							$(e.target).css('visibility', 'initial');
						}
					});
				}
			};
		}
	},

	game: {
		template: _.template($('#roomSummaryTemplate').html()),
		draw: function(game, room) {
			var target = game.num_rounds % room.num_players;
			if (target == 0) {
				target = room.num_players;
			}
			$('.player-div-inner-container').removeClass('playing');
			$('.player-div[data-position="'+target+'"] > .player-div-inner-container').addClass('playing');
			$('.cards-deck-left').text(game.cards_deck_left);
			$('#roomSummary').html(render_engine.game.template({game: game, room: room}));
		},

		good_guys_win: function() {

		},

		bad_guys_win: function() {

		}
	},

	gold: {
		get_image: function (count) {
			return '<img src="/static/games/images/golds/'+count+'.jpg">';
		},

		draw: function(golds) {
			var lis = [];
			for (var i = 0; i < golds.length; i++) {
				lis.push('<li data-gold_num="'+golds[i]+'">'+render_engine.gold.get_image(golds[i])+'</li>');
			};
			$('#goldsArea').html(lis.join(''));
		}
	}
}
