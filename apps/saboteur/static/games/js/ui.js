$(function() {

	function is_self_turn() {
		var mod = gameStatus.num_rounds % roomStatus.num_players;
		if (mod == 0) {
			mod = roomStatus.num_players
		}
		if (mod == myself.position) {
			return true;
		}
		alert(ERRORS.not_in_turn);
		return false;
	}

	$('.startGameBtn').button();
	$('.quitRoomBtn').button();

	//quit room
	$('.quitRoomBtn').on('vclick', function(){
		if (confirm('Are you sure to exit?')) {
			$.post('/ajax/rooms/exit', {room_id: window.ROOM}, function(data){
				window.location = '/';
			});
		}
	});

	//start game
	$('.startGameBtn').on('vclick', function(){
		var myself = $(this);
		if (!myself.attr('disabled')) {
			myself.button('disable');
			$.post('/ajax/games/start', {room_id: window.ROOM}, function(data){
				myself.button('enable');
			});
		}
	});
  
	$(document).on('vclick', ".cards", function(){	// tap to flip a card
		var myself = $(this);
		var  card_index = parseInt(myself.attr('data-id')) - 1;
		if (CARDS[card_index].card_type === 'ROAD') {
			myself.toggleClass('img-flipped');
		} else {
			// $('#toolsImageDialog img').attr('src', CARDS[card_index].image_path);
			// changePage('#toolsImageDialog');
		}
	});

	$(".cards").draggable({	// tap drag card
		revert: 'invalid',
		containment: "body",
		scroll: false,
		helper: 'clone',
		appendTo: 'body',
		start: function (e, ui) {
			$(e.target).css('visibility', 'hidden');
		}, stop: function (e, ui) {
			$(e.target).css('visibility', 'initial');
		}
	});
	$('#myRoleCover').on('tap', function(e){		// check my role
		e.preventDefault();
		if ($('#myRole').css('display') === 'none') {
			$('#myRole').css({
			'top': $('#myRoleCover').offset().top,
			'left': $('#myRoleCover').offset().left
			}).show();
			$('#myRoleCover').css('opacity', 0.1);
		} else {
			$('#myRoleCover').css('opacity', 1);
			$('#myRole').hide();
		}
	});


	//After a box of map is selected	
	$(".game-map-road-cell").on('vclick', function() {
		$('#disableTouchDiv').show();
		if (typeof window.playing_card_id === 'number') {
			var cell = $(this);
			var post_data = {
				room_id: window.ROOM, card_id: window.playing_card_id,
				x: cell.attr('data-x'), y: cell.attr('data-y')
			};
			switch (CARDS[window.playing_card_id-1].card_type) {
				case "ROAD":
					post_data['reversed'] = window.playing_card_flipped;
					break;
				case "RDMP":
				case "BKRD":
					break;
			}
			$.post('/ajax/cards/play', post_data, function(data){
				if (data.message) {
					alert(data.message);
				}
				if (data.success) {
					$('#myCards [data-id="'+window.playing_card_id+'"]').empty().remove();
					changePage('#mainActionPage');
				}
				$('#disableTouchDiv').hide();
			}, 'json');
		} else {
			changePage('#mainActionPage');
			$('#disableTouchDiv').hide();
		}
	});

	//After a role is selected
	$(".player-div").on('vclick', function() {
		$('#disableTouchDiv').show();
		if (typeof window.playing_card_id === 'number') {
			var cell = $(this);
			var post_data = {
				room_id: window.ROOM, card_id: window.playing_card_id,
				player_position: cell.attr('data-position')
			};
			$.post('/ajax/cards/play', post_data, function(data){
				if (data.success) {
					$('#myCards [data-id="'+window.playing_card_id+'"]').empty().remove();
				}
				changePage('#mainActionPage');
				$('#disableTouchDiv').hide();
			}, 'json');
		} else {
			changePage('#mainActionPage');
			$('#disableTouchDiv').hide();
		}
	});
	
	//After get card is pressed
	$("#drawCardAction").on('vclick', function() {
		$('#disableTouchDiv').show();
		$.post('/ajax/cards/draw', {room_id: window.ROOM}, function(data){
			if (data.success) {
				render_engine.myself.add_cards(data.card_id);
			}
			$('#disableTouchDiv').hide();
		}, 'json');
	});

	$("#discardAction").droppable({
		drop: function( event, ui ) {
			if (!is_self_turn()) {
				return;
			}
			$('#disableTouchDiv').show();
			var card_id = parseInt($(ui.draggable).attr('data-id'));
			$.post('/ajax/cards/discard', {room_id: window.ROOM, card_id: card_id},
				function(data){
					if (data.success) {
						$(ui.draggable).empty().remove();
					}
					$('#disableTouchDiv').hide();
				},
			'json');
		}
	});


    $("#playCardAction").droppable({
		drop: function(event, ui) {
			if (!is_self_turn()) {
				return;
			}
			var card_id = parseInt($(ui.draggable).attr('data-id'));
			window.playing_card_id = card_id;
			switch (CARDS[card_id-1].card_type) {
				case "ROAD":
					if (myself.affected_cards.length > 0) {
						alert(ERRORS.blocked);
						return;
					}
					window.playing_card_flipped = $(ui.draggable).hasClass('img-flipped') ? 1 : 0;
				case "RDMP":
				case "BKRD":
					changePage("#gameMapPage");
					break;
				case "UBLK":
				case "BLCK":
					changePage("#playersPage");
					break;
				default:
					break;
			}
		}
	});



	// pick gold
	$(document).on('vclick', '#goldsArea [data-gold_num]', function(){
		var gold_num = $(this).attr('data-gold_num');
		$.post('/ajax/golds/draw', {room_id: window.ROOM, gold_num: gold_num}, function(data){
			if (data.success) {

			}
		}, 'json');
	});

	$('.backToMainBtn').on('vclick', function(){
		changePage('#mainActionPage');
	});


	$('.passBtn').on('vclick', function(){
		if (is_self_turn()) {
			$.post('/ajax/games/pass',{room_id: window.ROOM}, function(){
				
			}, 'json');
		}
	});
});