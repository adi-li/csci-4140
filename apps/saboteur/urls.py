from django.conf.urls import patterns, url

urlpatterns = patterns(
    'saboteur.views',
    url(r'^games/waiting$', 'api', {'method': 'waiting', 'require_mobile': False}, name="api-game-waiting"),
    url(r'^rooms/exit$', 'api', {'method': 'exit_room', 'require_mobile': True}, name="api-room-exit"),
    url(r'^games/start$', 'api', {'method': 'start_game', 'require_mobile': True}, name="api-game-start"),
    url(r'^games/refresh$', 'api', {'method': 'refresh', 'require_mobile': False}, name="api-game-refresh"),
    url(r'^cards/play$', 'api', {'method': 'play_card', 'require_mobile': True}, name="api-card-play"),
    url(r'^cards/discard$', 'api', {'method': 'discard_card', 'require_mobile': True}, name="api-card-discard"),
    url(r'^cards/draw$', 'api', {'method': 'draw_card', 'require_mobile': True}, name="api-card-draw"),

    url(r'^games/waiting_next$', 'api', {'method': 'waiting_next', 'require_mobile': False}, name="api-game-waiting-next"),

    url(r'^golds/draw$', 'api', {'method': 'draw_gold', 'require_mobile': True}, name="api-gold-draw"),
    
    url(r'^games/pass$', 'api', {'method': 'pass_game', 'require_mobile': True}, name="api-game-pass"),
)
