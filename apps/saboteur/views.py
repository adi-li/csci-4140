from django.core.exceptions import PermissionDenied
from django.shortcuts import get_object_or_404
from django.utils.translation import ugettext as _

from utils.http import HttpJSONResponse
from saboteur import ajax
from rooms.models import GameRoom


def api(request, method, require_mobile):
    if request.method != 'POST':
        raise PermissionDenied

    result = {'success': False, 'message': ''}

    room_id = request.POST.get('room_id', '')
    if require_mobile:
        if not request.is_mobile or request.player is None or \
                request.player.room.id_hash != room_id:
            result['message'] = _('You are not the one in this room')
            return HttpJSONResponse(result)
    room = get_object_or_404(GameRoom, id_hash=room_id)

    func = getattr(ajax, method, 'error')

    if callable(func):
        result = func(request, result, room)
    else:
        result['message'] = _('Internal Error, will be fixed as soon as posible')
    return HttpJSONResponse(result)
