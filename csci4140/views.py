from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext

from saboteur.models import Game, Player
from rooms.models import GameRoom


def home(request):
    """
    check player exist => go to room
    else => give a form to create room OR list public rooms
    """
    if request.is_mobile:
        if request.player is not None:
            return redirect('game-room', room_id=request.player.room.id_hash)

    rooms = GameRoom.public_objects.get_all_avaliable()
    return render_to_response('home.html', {'rooms': rooms},
                              RequestContext(request))
