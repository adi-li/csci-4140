from django.contrib import admin


class BaseAdmin(admin.ModelAdmin):
    list_per_page = 30
    date_hierarchy = 'created'


class IdHashAdmin(BaseAdmin):
    readonly_fields = ('id_hash',)
