from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext as _


class IdHashField(forms.RegexField):
    """
    only accept HEX words
    """
    def __init__(self, *args, **kwargs):
        super(IdHashField, self).__init__(
            '^[0-9A-Fa-f]{5,50}$', 50, 5,
            _('Invalid Id'), *args, **kwargs)


class PhoneField(forms.RegexField):
    """
    only accept international phone number
    """
    def __init__(self, *args, **kwargs):
        super(PhoneField, self).__init__(
            '^[0-9\-\+]{4,28}$', None, None,
            _('Please enter a valid phone number'), *args, **kwargs)


class ModelField(forms.CharField):
    def __init__(self, model, field='id', check_exist=True, *args, **kwargs):
        super(ModelField, self).__init__(*args, **kwargs)
        if 'not_exist' in self.error_messages:
            self.error_messages['not_exist'] = _('Object does not exist')
        self.model, self.field, self.check_exist = model, field, check_exist

    def to_python(self, value):
        value = super(ModelField, self).to_python(value)
        if not value:
            if self.check_exist:
                raise ValidationError(self.error_messages['not_exist'])
            return None
        args = {self.field: value}
        try:
            obj = self.model.objects.get(**args)
        except self.model.DoseNotExist:
            if self.check_exist:
                raise ValidationError(self.error_messages['not_exist'])
            return None
        else:
            return obj
