from django.utils.translation import ugettext as _


def errors_to_dict(errors):
    """
    Convert a Form error list to JSON::
        >>> f = SomeForm(...)
        >>> errors_to_dict(f.errors)
        {'field': ['This field is required']}
    """
    # Force error strings to be un-lazied.
    return dict(
        (k, map(unicode, v))
        for (k, v) in errors.iteritems()
    )


def errors_to_string(form_errors, fields=(), allowed=False):
    """
    Convert a Form error list to string::
        >>> f = SomeForm(...)
        >>> errors_to_string(f.errors)
        {'field': ['This field is required']}
    """
    errors = []
    for key in form_errors.iterkeys():
        if key in fields != allowed:
            for error in form_errors[key]:
                errors.append(_('{field}: {error}').format(field=key, error=error))
    return '\n'.join(errors)
