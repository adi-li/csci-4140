def empty(var):
    """ return True if var is the following
        None
        0 (0 as an integer)
        0L (0 as a long integer)
        0.0 (0 as a float)
        0j (0 as a complex)
        False
        "" (an empty string)
        "0" (0 as a string)
        u"0" (0 as a unicode string)
        {} (an empty dict)
        [] (an empty list)
        () (an empty tuple)
    """
    if var == '0':
        return True
    return not bool(var)


def mail_admins_with_var(request=None):
    from django.conf import settings
    # don't send in debug mode, since it should be observed in html already
    if settings.DEBUG:
        return

    from django.core.mail import mail_admins
    import sys
    import traceback

    try:
        # Mail the admins with the error
        from django.views.debug import ExceptionReporter
        from django.http import HttpRequest
        from django.core.mail import EmailMultiAlternatives
        if not request:
            request = HttpRequest()
            request.META['REMOTE_ADDR'] = 'UNKNOWN'
            request.META['SERVER_NAME'] = 'FAKE'
            request.META['SERVER_PORT'] = '80'

        exp = sys.exc_info()
        t = ExceptionReporter(request, *exp)

        subject = "{subject_prefix}[{remote_addr} => {domain}] Exception: {expection_name}".format(
            subject_prefix=settings.EMAIL_SUBJECT_PREFIX,
            remote_addr=request.META['REMOTE_ADDR'],
            domain=settings.DOMAIN,
            expection_name=str(exp[1])
        )

        from_email = settings.SERVER_EMAIL
        to = map(lambda x: x[1], settings.ADMINS)

        text_content = 'Traceback:\n{traceback}\n\n'.format(traceback='\n'.join(traceback.format_exception(*exp)))
        html_content = t.get_traceback_html()
        msg = EmailMultiAlternatives(subject, text_content, from_email, to)
        msg.attach_alternative(html_content, "text/html")
        msg.send()

    except Exception, e:
        try:
            # Mail the admins with the error
            exc_info = sys.exc_info()
            subject = 'Uncaught exception'
            try:
                request_repr = repr(request)
            except:
                request_repr = 'Request repr() unavailable'
            import traceback
            message = 'Traceback:\n{traceback}\n\nRequest:\n{request}'.format(
                traceback='\n'.join(traceback.format_exception(*exc_info)),
                request=request_repr)
            mail_admins(subject, message, fail_silently=True)

        except Exception, e:
            mail_admins("SERIOUS ERROR", "Not sure what happened.. %s" % str(e), fail_silently=True)


def get_remote_address(request):
    x = request.META.get('HTTP_X_FORWARDED_FOR')
    if x:
        ip = x.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip
