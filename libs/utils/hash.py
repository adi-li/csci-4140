from hashlib import sha1
from random import randrange
from django.conf import settings
from time import time
import math


def generate_hash(num, salt=''):
    return sha1(settings.SECRET_KEY + str(num) + salt).hexdigest()


class VaryLengthHashGenerator(object):
    def __init__(self, alphabet='abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-_'):
        super(VaryLengthHashGenerator, self).__init__()
        self.ALPHABET = alphabet
        self.BASE = len(self.ALPHABET)
        self.MAXLEN = 5

    def encode_id(self, n):
        pad = self.MAXLEN - 1
        n = int(n + pow(self.BASE, pad))

        s = []
        t = int(math.log(n, self.BASE))
        while True:
            bcp = int(pow(self.BASE, t))
            a = int(n / bcp) % self.BASE
            s.append(self.ALPHABET[a:a + 1])
            n = n - (a * bcp)
            t -= 1
            if t < 0:
                break

        return "".join(reversed(s))

    def decode_id(self, n):

        n = "".join(reversed(n))
        s = 0
        l = len(n) - 1
        t = 0
        while True:
            bcpow = int(pow(self.BASE, l - t))
            s = s + self.ALPHABET.index(n[t:t + 1]) * bcpow
            t += 1
            if t > l:
                break

        pad = self.MAXLEN - 1
        s = int(s - pow(self.BASE, pad))

        return int(s)
