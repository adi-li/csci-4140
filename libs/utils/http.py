from django.http import HttpResponse
from django.utils import simplejson as json


class HttpJSONResponse(HttpResponse):
    def __init__(self, content='', mimetype=None, status=None,
            content_type=None, json_encoded=False, *args, **kwargs):
        if not mimetype:
            mimetype = 'application/json'
        if not json_encoded:
            content = json.dumps(content)
        super(HttpJSONResponse, self).__init__(content, mimetype=mimetype,
            status=status, content_type=content_type, *args, **kwargs)