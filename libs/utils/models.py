from utils.hash import generate_hash
from django.db import models


# -------------------- BaseModel --------------------
class BaseModel(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        get_latest_by = 'created'
        ordering = ['-created']
        abstract = True


# -------------------- IdHashModel --------------------
class IdHashModel(BaseModel):
    id_hash = models.CharField(max_length=50, unique=True,
                               null=True, db_index=True)

    def save(self, *args, **kwargs):
        if not self.pk:
            super(IdHashModel, self).save(*args, **kwargs)
            self.save()
        elif not self.id_hash:
            self.id_hash = generate_hash(self.pk, self.__class__.__name__)
            try:
                super(IdHashModel, self).save(*args, **kwargs)
            except:
                self.save()
            self.save()
        else:
            super(IdHashModel, self).save(*args, **kwargs)

    class Meta(BaseModel.Meta):
        abstract = True
