import celery
from djcelery.models import PeriodicTask
from django.utils import timezone
from django.core.cache import cache


class RunOncePeriodicTask(celery.task.PeriodicTask):
    abstract = True
    ignore_result = True
    relative = False
    options = None
    compat = True

    def __call__(self, *args, **kwargs):
        if cache.add(self.name, True, 30):
            try:
                t = PeriodicTask.objects.using('default').select_for_update().filter(name=self.name)[0]
            except:
                print 'FAIL, no task for: %s' % self.name
            else:
                if self.run_every.is_due(t.last_run_at):
                    t.last_run_at = timezone.now()
                    t.save()
                    return self.run(*args, **kwargs)
                else:
                    print 'FAIL, task already done: %s' % self.name
        else:
            print 'FAIL, no locker for: %s' % self.name


def run_once_periodic_task(*args, **options):
    return celery.task.periodic_task(**dict({'base': RunOncePeriodicTask}, **options))
